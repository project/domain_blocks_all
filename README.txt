
-- SUMMARY --

The Domain Blocks All module creates a bulk block management form on the
Domain Access admin page called "Enable Blocks" to easily enable multiple
blocks to multiple domains. This module depends on the Domain Access and
Domain Blocks modules.

The use case is when you have organized your current blocks into regions on
your theme and just need to enable multiple blocks to a new Domain Access
record on your site.

For a full description of the module, visit the project page:
  http://drupal.org/project/domain_blocks_all

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/domain_blocks_all


-- REQUIREMENTS --

None.
 
 
-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* User must have 'administer domains' permission.


-- TROUBLESHOOTING --

* If the menu item does not display, try clearing cache and refreshing the page.


-- CONTACT --

Current maintainers:
* Aaron Deutsch (aisforaaron) - http://drupal.org/user/116113