<?php
/**
 * Domain Blocks All
 */

/**
 * Implementation of hook_init().
 */
function domain_blocks_all_init() {
}

/**
 * Implementation of hook_menu().
 */
function domain_blocks_all_menu() {
  $items = array();
  $items['admin/build/domain/blocks-all'] = array(
    'title' => t('Enable Blocks'),
    'description' => 'Domain bulk block management',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_blocks_all_bulk_blocks_form'),
    'access arguments' => array('administer domains'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -7
   );
  return $items;
}

/**
 * Form to manage bulk domain block management
 */
function domain_blocks_all_bulk_blocks_form(){
  $form = array();
  // domains as checkboxes
  $form['domains_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domains list'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#prefix' => 'Enable blocks for domain access sites in bulk. <BR><BR>'
  );
  // create form element, this will print a list of checkboxes by subdomain
  $form['domains_group']['domains'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select at least one domain'),
    '#description' => t('Select individual domains to enable blocks.'),
    '#options' => domain_blocks_all_get_domains()
  );
  // blocks as checkboxes
  $form['blocks_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blocks list'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // get all blocks and pass as options
  $blocks = _block_rehash();
  $block_options = array();
  foreach ($blocks as $block) {
    $block_options[$block['delta']] = $block['info'];
  }
  $form['blocks_group']['blocks'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select at least one block'),
    '#description' => t('Select individual blocks to enable.'),
    '#options' =>  $block_options 
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Run'),
  );
  return $form;
}

/**
 * Implementation of hook_form_submit().
 */
function domain_blocks_all_bulk_blocks_form_submit($form, &$form_state) {
  // loop through domains checked
  $selected_domain_ids = array();
  foreach ($form_state['values']['domains'] as $domain) {
    if ($domain != FALSE) {
      // switch primary domain back from -1 to zero
      if ($domain == -1) {
        $domain = 0;
      }
      $selected_domain_ids[] = $domain;
    }
  }

  // loop through blocks
  $selected_block_deltas = array();
  foreach ($form_state['values']['blocks'] as $block) {
    if ($block != FALSE) {
      $selected_block_deltas[] = $block;
    }
  }

  // get module name for each delta
  $block_list = _block_rehash();
  foreach($block_list as $block) {
    $block_module_by_delta[$block['delta']] = $block['module'];
    $block_info_by_delta[$block['delta']] = $block['info'];
  }

  // update db
  // loop through domains
  // for each domain, loop through all blocks checked and run one insert per block
  $dsm = ''; // drupal confirm message
  foreach ($selected_domain_ids as $single_domain_id) {
    foreach($selected_block_deltas as $block_delta) {
      $module = $block_module_by_delta[$block_delta];
      $delta = $block_delta;
      $realm = 'domain_id';

      // check if record already exists
      // if not, add the record
      if(!db_result(db_query("SELECT COUNT(*) FROM {domain_blocks} WHERE module = '%s' AND delta = '%s' AND realm = '%s' AND domain_id = '%d'", $module, $delta, $realm, $single_domain_id))) {
        db_query("INSERT INTO {domain_blocks} (module, delta, realm, domain_id) VALUES ('%s', '%s', '%s', %d)", $module, $delta, $realm, $single_domain_id);
        $dsm .= $block_info_by_delta[$delta].', ';
      }
    }
  }
  drupal_set_message("Blocks updated: ".$dsm);
}

/**
 * Get all domains as id=>name in alpha sort
 * Taken and modified from domain_sendall.module to show all domains
 * 
 * @return
 *   array with domains
 */
function domain_blocks_all_get_domains() {
  $all_domains = array();
  $result = db_query('SELECT * FROM {domain}');
  while ($domains = db_fetch_array($result)) {
    // Primary domain will have a domain_id of zero,
    // which also counts as FALSE in a bool check
    // so we replace that with a different value, -1
    if ($domains['domain_id'] == 0) {
      $all_domains[-1] = t($domains['sitename']);
    }
    else {
      $all_domains[$domains['domain_id']] = t($domains['sitename']);
    }
  }
  // alpha sort list, keep key/value assoc
  asort($all_domains);
  return $all_domains;
}
